var autoprefixer = require('autoprefixer');
var precss = require('precss');
var verticalRhythm = require('postcss-lh');
var responsiveType = require('postcss-responsive-type');
var minmax = require('postcss-media-minmax');

var plugins = [
  verticalRhythm({
    lineHeight: 1
  }),
  responsiveType(),
  precss({
    variables: {
      variables: require('../src/utilities')
    }
  }),
  minmax(),
  autoprefixer({
    browsers: [
      '>1%',
      'last 4 versions',
      'Firefox ESR',
      'not ie < 9', // React doesn't support IE8 anyway
    ]
  })
]
module.exports = plugins;
