export default {
  interest: {
    fixedRate: 3.5,
    variableRate: 4.5
  }
}
