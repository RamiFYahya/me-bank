const steps = {
  applicant: {
    id: 'applicant',
    active: true,
    completed: false
  },
  purpose: {
    id: 'purpose',
    active: false,
    completed: false
  },
  property: {
    id: 'property',
    active: false,
    completed: false
  },
  finances: {
    id: 'finances',
    active: false,
    completed: false
  },
  borrow: {
    id: 'borrow',
    active: false,
    completed: false
  }
}

export default steps
