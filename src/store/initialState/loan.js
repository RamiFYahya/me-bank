import { MONTHLY } from '../../constants/frequency';
import { FLEXIBLEMP } from '../../constants/productCodes';
import { PARAMETERS } from '../../constants';

export default {
  type: {
    applicant: '',
    application: ''
  },
  property: {
    value: '',
    purpose: PARAMETERS.PERSONAL,
    description: PARAMETERS.ESTABLISHED_HOME,
    state: 'VIC'
  },
  finances: {
    income: '',
    incomeFrequency: MONTHLY,
    expenses: '',
    expensesFrequency: 'month'
  },
  borrow: {
    amount: '400000'
  },
  split: {
    active: true,
    fixedAmount: '200000',
    fixedDuration: '2',
  },
  mortgage: {
    term: '10',
    frequency: MONTHLY,
    activeProduct: FLEXIBLEMP
  },
  repayment: {
    interestOnly: true,
    interestOnlyDuration: '2'
  }
}
