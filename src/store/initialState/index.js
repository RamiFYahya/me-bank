import steps from './steps';
import loan from './loan';
import mortgage from './mortgage';

export default {
  steps,
  loan,
  mortgage
}
