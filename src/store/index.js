import INTITIAL_STATE from './initialState'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from '../reducers'

export default createStore(rootReducer, INTITIAL_STATE, composeWithDevTools(applyMiddleware(thunk)))
