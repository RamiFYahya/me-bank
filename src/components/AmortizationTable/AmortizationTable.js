import React from 'react';
import './AmortizationTable.css';
import Decimal from 'decimal.js';
import { currencyFormat } from '../../utilities/formatter';
import keys from 'lodash/keys';
import head from 'lodash/head';
import values from 'lodash/values';
import S from 'string';

const AmortizationTable = ({ data }) => {
  const dataKeys = keys(head(data));
  return (
    <table className="AmortizationTable">
      <thead>
        <tr>
          {dataKeys.map((key, index) => (
            <th key={index}>{S(key).humanize().titleCase().s}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((obj, index) => {
          return (
            <tr key={index}>
              {values(obj).map((value, index) => (
                <td key={index}>
                  { index === 0 ? value : currencyFormat(Decimal(value).round().toNumber()) }
                </td>
              ))}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default AmortizationTable
