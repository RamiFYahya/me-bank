import React from 'react'
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis } from 'recharts'
import { currencyFormat } from '../../utilities/formatter'
import './IncomeExpensesChart.css'

const IncomeExpensesChart = ({ expenses, income, repayment, balance }) => {
  const data = [
    {
      expenses: expenses,
      repayment: repayment,
      balance: balance
    }
  ];

  return (
    <div className="IncomeExpensesChart">
      <div className="IncomeExpensesChart-wrapper">
        <div className="IncomeExpensesChart-label" style={{color: '#ff344d'}}>expenses {currencyFormat(expenses)}</div>
        <div className="IncomeExpensesChart-label" style={{textAlign: 'right'}}>income {currencyFormat(income)}</div>
      </div>

      <ResponsiveContainer width={'100%'} height={20}>
        <BarChart data={data} layout="vertical" margin={{left: 0}}>
          <XAxis type="number" hide={true} domain={[0, 'dataMax']}/>
          <YAxis type="category" hide={true} />
          <Bar barSize={8} dataKey="balance" stackId="a" fill="#d9dfe2" isAnimationActive={false}></Bar>
          <Bar barSize={8} dataKey="repayment" stackId="a" fill="#0995d6" isAnimationActive={false}></Bar>
          <Bar barSize={8} dataKey="expenses" stackId="a" fill="#ff344d" isAnimationActive={false}></Bar>
        </BarChart>
      </ResponsiveContainer>
      <div className="IncomeExpensesChart-label">
        remaining balance {currencyFormat(balance)}
      </div>

    </div>
  )
}

export default IncomeExpensesChart
