import React, { PropTypes } from 'react';
import classNames from 'classnames';
import SectionIntro from '../SectionIntro';
import './Section.css';

const Section = ({ sectionContent, children, inverted }) => {
  const {
    intro,
    background
  } = sectionContent;

  const cls = classNames({
    'Section': true,
    'inverted': inverted
  })

  return (
    <div
      className={cls}
      style={{background: background}}
    >
      <div className="Section-container">
        <div className="Section-intro">
          <SectionIntro title={intro.title} subtitle={intro.subtitle} />
        </div>
        <div className="Section-wrapper">
          {children}
        </div>
      </div>
    </div>
  )
}

Section.propTypes = {
  sectionContent: PropTypes.shape({
    intro: PropTypes.shape({
      title: PropTypes.string.isRequired,
      subtitle: PropTypes.string
    }),
    background: PropTypes.string.isRequired
  }),
  children: PropTypes.element.isRequired,
  inverted: PropTypes.bool
}

Section.defaultProps = {
  inverted: false
}

export default Section;
