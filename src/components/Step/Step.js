import React, { PropTypes } from 'react';
import classNames from 'classnames';
import './Step.css';

const Step = ({ children, active }) => {
  const cls = classNames({
    'Step': true,
    'Step-disabled': active === false
  })

  return (
    <div className={cls}>
      {children}
    </div>
  )
};

Step.propTypes = {
  children: PropTypes.node.isRequired,
  active: PropTypes.bool
}

Step.defaultProps = {
  active: false
}

export default Step;
