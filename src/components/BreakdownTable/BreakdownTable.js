import React from 'react';
import { formatNumber } from '../../utilities/formatter';
import './BreakdownTable.css';

const BreakdownTable = ({ deposit, costs, refund }) => (
  <div className="BreakdownTable">
    {deposit &&
      <div className="BreakdownTable-deposit BreakdownTable-row BreakdownTable-bold">
        <div className="BreakdownTable-label">
          {deposit.label}
        </div>
        <div className="BreakdownTable-amount">
          {formatNumber(deposit.amount, false)}
        </div>
      </div>
    }
    {costs &&
      <ul className="BreakdownTable-costs">
        {costs.map((cost, index) => (
          <li key={index} className="BreakdownTable-row">
            <div className="BreakdownTable-label">
              {cost.label}
            </div>
            <div className="BreakdownTable-amount">
              {formatNumber(cost.amount, false)}
            </div>
          </li>
        ))}
      </ul>
    }
    {refund &&
      <div className="BreakdownTable-refund BreakdownTable-row BreakdownTable-bold">
        <div className="BreakdownTable-label">
          {refund.label}
        </div>
        <div className="BreakdownTable-amount">
          {formatNumber(refund.amount, false)}
        </div>
      </div>
    }
  </div>
)

export default BreakdownTable;
