import React from 'react';
import head from 'lodash/head';
import Decimal from 'decimal.js';
import './AmortizationChart.css';
import { ResponsiveContainer, AreaChart, Area, XAxis, Tooltip } from 'recharts';
import { currencyFormat } from '../../utilities/formatter';
import ChartTooltip from '../ChartTooltip';

const AmortizationChart = ({ schedule }) => {
  const scheduleItem = head(schedule);
  const totalRepayment = scheduleItem.amountOwing;
  const principalBalance = scheduleItem.principalBalance;
  const totalInterest = Decimal.sub(totalRepayment, principalBalance);

  return (
    <div>
      <ResponsiveContainer
        width={'100%'}
        height={260}
      >
        <AreaChart
          data={schedule}
          margin={{ top: 0, right:0, left: 0, bottom: 0}}
        >
          <Area
            type="monotone"
            dataKey="interestOwing"
            stackId="1"
            fillOpacity={0.85}
            fill="#fff"
            dot={{
              stroke: '#fff',
              strokeWidth: 1,
              fill: '#03a9f4',
              r: 4
            }}
            isAnimationActive={false}
          />
          <Area
            type="monotone"
            dataKey="principalBalance"
            stackId="1"
            fillOpacity={0.85}
            fill="#03a9f4"
            isAnimationActive={false}
            activeDot={false}
          />
          <XAxis
            dataKey="paymentNumber"
            padding={{left: 10, right: 10, top: 0, bottom: 0}}
            interval={4}
          />
          <Tooltip content={<ChartTooltip />}/>
        </AreaChart>
      </ResponsiveContainer>
      <div className="AmortizationChart-wrapper">
        <div className="AmortizationChart-totals">
          <div className="AmortizationChart-label">total interest</div>
          <div className="h5">{currencyFormat(totalInterest)}</div>
        </div>
        <div className="AmortizationChart-totals">
          <div className="AmortizationChart-label">total loan repayment</div>
          <div className="h5">{currencyFormat(totalRepayment)}</div>
        </div>
      </div>
    </div>
  )
}

export default AmortizationChart
