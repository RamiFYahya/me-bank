import React, { PropTypes } from 'react';
import './Select.css';

const Select = ({children}) => (
  <span className="Control-select-wrapper">
    <span className="Control-select-arrow"></span>
    {children}
  </span>
)

Select.propTypes = {
  children: PropTypes.node.isRequired
}

export default Select;
