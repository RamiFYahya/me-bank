import React from 'react'
import MuiSlider from 'material-ui/Slider'
import toNumber from 'lodash/toNumber';
import { SLIDER } from '../../constants'
import './Slider.css'

const sliderStyle = {
  marginTop: 0,
  marginBottom: '1rem'
}

const Slider = ({ term, model, onSliderChange, fetchInterest }) => {
  return (
    <div className="Slider">
      <label>over a term of <span className="Slider-term"> {term} {term === 1 ? `year` : `years`} </span></label>
      <div className="Slider-wrapper">
        <MuiSlider
          defaultValue={toNumber(term)}
          value={toNumber(term)}
          max={SLIDER.MAX}
          min={SLIDER.MIN}
          step={1}
          onChange={(event, value) => {
            onSliderChange(model, value)
          }}
          sliderStyle={sliderStyle}
          className="Slider-input"
        />
      </div>
    </div>
  )
}

export default Slider
