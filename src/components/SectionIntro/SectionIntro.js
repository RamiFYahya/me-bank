import React, { PropTypes } from 'react';
import './SectionIntro.css';

const SectionIntro = ({ title, subtitle }) => (
  <div className="SectionIntro">
    <div className="SectionIntro-title h6">{title}</div>
    <p>{subtitle}</p>
  </div>
)

SectionIntro.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired
}

export default SectionIntro;
