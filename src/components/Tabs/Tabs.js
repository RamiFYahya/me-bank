import React from 'react';
import classNames from 'classnames';
import './Tabs.css'

const Tabs = ({ tabs, active, onTabClick, fetchInterest }) => {
  return (
    <div className="Tabs">
      {
        Object.keys(tabs).map((key) => {

          const tabClass = classNames({
            'Tab': true,
            'Tab-active': (active === tabs[key].id)
          })

          return (
            <div
              className={tabClass}
              key={tabs[key].id}
              onClick={() => {
                onTabClick(tabs[key].id)
                fetchInterest()
              }}
            >
              <div>{tabs[key].label}</div>
              {tabs[key].subLabel && <div className="Tab-sub-label">{tabs[key].subLabel}</div>}
            </div>
          )
        })
      }
    </div>
  )
};

export default Tabs;
