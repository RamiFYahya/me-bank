import React, { PropTypes } from 'react';
import classNames from 'classnames';
import './Button.css';

const Button = ({children, kind, size, ...other}) => {

  const cls = classNames({
    'Button': true,
    'Button-primary': kind === 'primary',
    'Button-secondary': kind === 'secondary',
    'Button-large': size === 'large',
  })

  return (
    <button
      className={cls}
      {...other}
    >
      {children}
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  size: PropTypes.string,
  kind: PropTypes.string
}

Button.defaultProps = {
  kind: 'primary'
}

export default Button
