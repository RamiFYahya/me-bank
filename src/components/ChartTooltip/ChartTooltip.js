import React from 'react';
import { currencyFormat } from '../../utilities/formatter';
import './ChartTooltip.css';

const ChartTooltip = ({payload}) => {
  const data = payload[0].payload;
  return (
    <div className="ChartTooltip">
      <ul>
        <li className="ChartTooltip-heading">Year: {data.paymentNumber}</li>
        <li>Principal: {currencyFormat(data.principalBalance)}</li>
        <li>Owing: {currencyFormat(data.amountOwing)}</li>
      </ul>
    </div>
  )
}

export default ChartTooltip;
