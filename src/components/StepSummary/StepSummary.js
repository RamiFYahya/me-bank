import React from 'react';
import './StepSummary.css';

const StepSummary = ({ title, background, editStep, stepId }) => (
  <div className="StepSummary"
    style={{
      background: background
    }}
  >
    <div className="StepSummary-container">
      <div className="StepSummary-title h4">{title}</div>
      <span
        className="StepSummary-link"
        onClick={() => editStep(stepId)}
      >
        edit
      </span>
    </div>
  </div>
)

export default StepSummary;
