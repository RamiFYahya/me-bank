import React, { PropTypes, Component } from 'react';
import classNames from 'classnames';
import uniqueId from 'lodash/uniqueId';
import './Checkbox.css'

class Checkbox extends Component {
  componentWillMount() {
    this.setState({
      randomId: uniqueId('checkbox-')
    })
  }

  render() {
    const cls = classNames({
      'Checkbox': true,
      'Checkbox-large': this.props.size === 'large'
    })

    return (
      <div className={cls}>
        <input type="checkbox" className="Checkbox-control" id={this.state.randomId} {...this.props} />
        <label htmlFor={this.state.randomId}>{this.props.label}</label>
      </div>
    )
  }
}

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  size: PropTypes.string
}

export default Checkbox;
