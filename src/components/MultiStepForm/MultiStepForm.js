import React from 'react';
import './MultiStepForm.css';

const MultiStepForm = ({ children }) => (
  <div className="MultiStepForm">
    {children}
  </div>
);

export default MultiStepForm;
