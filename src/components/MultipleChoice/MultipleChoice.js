import React, { PropTypes } from 'react';
import last from 'lodash/last';
import classNames from 'classnames';
import SectionIntro from '../SectionIntro';
import './MultipleChoice.css';

const getClassNames = (selectedOption, currentOption) => {
  return classNames({
    'MultipleChoice-option': selectedOption === '' || selectedOption === currentOption,
    'MultipleChoice-option is-inactive': selectedOption !== '' && selectedOption !== currentOption
  })
}

const MultipleChoice = ({ content, model, onChange, activateStep, completeStep, stepId, nextStepId, selectedOption }) => {
  const {
    intro,
    options
  } = content;

  return (
    <div className="MultipleChoice">
      <div className="MultipleChoice-bg-extend-pre" style={{background: intro.background}}></div>
      <div className="MultipleChoice-container">
        <div className="MultipleChoice-intro" style={{background: intro.background}}>
          <SectionIntro title={intro.title} subtitle={intro.subtitle} />
        </div>
        <div className="MultipleChoice-options">
          { options.map((option, index) => (
            <div
              className={getClassNames(selectedOption, option.value)}
              key={index}
              style={{
                background: option.background
              }}
              onClick={() => {
                onChange(model, option.value)
                completeStep && completeStep(stepId)
                activateStep && activateStep(nextStepId)
              }}
            >
              {option.icon && <div><img src={option.icon} alt=""/></div>}
              <div className="h5">{option.title}</div>
              <p>{option.subtitle}</p>
            </div>
          ))}
        </div>
      </div>
      <div className="MultipleChoice-bg-extend-post" style={{background: last(options).background}}></div>
    </div>
  )
};

MultipleChoice.propTypes = {
  content: PropTypes.shape({
    intro: PropTypes.shape({
      title: PropTypes.string.isRequired,
      subtitle: PropTypes.string,
      background: PropTypes.string.isRequired
    }),
    options: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      subtitle: PropTypes.string,
      background: PropTypes.string.isRequired,
      icon: PropTypes.string,
      value: PropTypes.string.isRequired
    }))
  }),
  model: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  activateStep: PropTypes.func,
  completeStep: PropTypes.func,
  stepId: PropTypes.string,
  nextStepId: PropTypes.string,
  selectedOption: PropTypes.string.isRequired
}

export default MultipleChoice;
