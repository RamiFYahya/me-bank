import React, { PropTypes } from 'react';
import './ProductDetails.css';

const ProductDetails = ({ product }) => {
  return (
    <div className="ProductDetails">
      <ul>
        {
          product.features.map((feature, index) => {
            return (
              <li key={index}>{feature.desc}</li>
            )
          })
        }
      </ul>
      <a href={product.link} target="_blank" className="ProductDetails-link">view more information</a>
    </div>
  )
}

ProductDetails.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.string,
    features: PropTypes.array.isRequired,
    link: PropTypes.string.isRequired
  }).isRequired
}

export default ProductDetails;
