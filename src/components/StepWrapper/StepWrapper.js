import React, {PropTypes} from 'react';
import classNames from 'classnames';
import './StepWrapper.css';

const StepWrapper = ({ children, inverted }) => {
  const cls = classNames({
    StepWrapper: true,
    'fixed-height': true,
    inverted
  });

  return (
    <div className={cls}>
      {children}
    </div>
  )
}

StepWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  inverted: PropTypes.bool
}

StepWrapper.defaultProps = {
  inverted: false
}

export default StepWrapper;
