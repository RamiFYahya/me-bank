import React, { PropTypes } from 'react';
import './ControlWrapper.css';

const ControlWrapper = ({ label, controlType, controlStyle, children }) => (
  <div
    className={
      `ControlWrapper ${controlStyle === 'stacked' ? 'ControlWrapper-stacked' : ``} ${controlStyle === 'horizontal' ? 'ControlWrapper-horizontal' : ``}
      `
    }>
    {
      label.position === 'pre' &&
      <label className="Control-label">{label.text}</label>
    }
      <span className={`Control-field ${controlType === 'currency' ? 'Control-field-currency' : ''}`}>
        {children}
      </span>
    {
      label.position === 'post' &&
      <label className="Control-label">{label.text}</label>
    }
  </div>
)

ControlWrapper.propTypes = {
  children: PropTypes.node.isRequired
}

export default ControlWrapper;
