import React, { PropTypes, Component } from 'react';
import uniqueId from 'lodash/uniqueId';
import './Radio.css';

class Radio extends Component {
  componentWillMount() {
    this.setState({
      randomId: uniqueId('radio-')
    })
  }

  render() {
    return (
      <div className="Radio">
        <label htmlFor={this.state.randomId}>
          <input type="radio" className="Radio-control" id={this.state.randomId} {...this.props} />
          <span className="Radio-state"></span>
          {this.props.label}
        </label>
      </div>
    )
  }
}

Radio.propTypes = {
  label: PropTypes.string.isRequired
}

export default Radio;
