import React from 'react';
import './Header.css';

const Header = ({ title, subtitle }) => (
  <div className="Header">
    <div className="Header-container">
      <div className="h1">{title}</div>
      <div className="Header-subtitle">{subtitle}</div>
    </div>
  </div>
)

export default Header
