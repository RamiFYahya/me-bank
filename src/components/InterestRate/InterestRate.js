import React, { PropTypes } from 'react';
import './InterestRate.css';

const InterestRate = ({ rate, prefix, fontSize }) => (
  <span className="InterestRate" style={{fontSize}}>
    {prefix && <span className="InterestRate-prefix">{prefix}</span>}
    <span className="InterestRate-rate">{rate}</span>
    <span className="InterestRate-suffix">
      <span className="InterestRate-suffix-percentage">%</span>
      <span className="InterestRate-suffix-frequency">p.a</span>
    </span>
  </span>
);

InterestRate.propTypes = {
  rate: PropTypes.number.isRequired,
  prefix: PropTypes.string,
  fontSize: PropTypes.string.isRequired
}

export default InterestRate;
