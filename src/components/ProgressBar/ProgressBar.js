import React from 'react';
import './ProgressBar.css';
import logo from '../../assets/logo.svg';
import LinearProgress from 'material-ui/LinearProgress';

const ProgressBar = ({ percentageCompleted }) => (
  <div>
    <div className="l-preheader">
      <div className="AppTitle">
        <img src={logo} alt="ME Bank" className="AppTitle-logo" />
        <div className="h6">Home Loan Application</div>
      </div>
      <div className="PercentageComplete">
        {percentageCompleted}% Completed
      </div>
    </div>
    <div className="ProgressBar">
      <LinearProgress mode="determinate" value={percentageCompleted} />
    </div>
  </div>
);

export default ProgressBar;
