import { BASIC, FLEXIBLE, FLEXIBLEMP } from '../constants/productCodes'

const products = {
  [BASIC]: {
    id: BASIC,
    features: [
      {
        desc: "No fees to redraw from available funds"
      },
      {
        desc: "Start with as little as 5% deposit"
      },
      {
        desc: "Variable interest rates"
      }
    ],
    link: "https://mebank.com.au/personal/home-loans/compare-home-loans/basic-home-loan/"
  },
  [FLEXIBLE]: {
    id: FLEXIBLE,
    features: [
      {
        desc: "100% offset transaction facility"
      },
      {
        desc: "Option to fix the interest rate"
      },
      {
        desc: "No application fee or account-keeping fee"
      }
    ],
    link: "https://mebank.com.au/personal/home-loans/compare-home-loans/flexible-home-loan/"
  },
  [FLEXIBLEMP]: {
    id: FLEXIBLEMP,
    features: [
      {
        desc: "Lower variable interest rate"
      },
      {
        desc: "No solicitor or valuation fees"
      },
      {
        desc: "100% offset available"
      }
    ],
    link: "https://mebank.com.au/personal/home-loans/compare-home-loans/flexible-home-loan/"
  }
};

export default products;
