import productFFL from '../../assets/icons/product-ffl.svg';
import productFFLMP from '../../assets/icons/product-fflmp.svg';
import productBHL from '../../assets/icons/product-bhl.svg';
import { BASIC, FLEXIBLE, FLEXIBLEMP } from '../../constants/productCodes';

export default {
  intro: {
    title: `Select your product.`,
    subtitle: `Let's check the product is right for you.`,
    background: '#f7f7f7'
  },
  options: [
    {
      icon: productBHL,
      title: 'Basic home loan.',
      background: '#fff3e3',
      value: BASIC,
    },
    {
      icon: productFFL,
      title: 'Flexible home loan.',
      background: '#e1f2fb',
      value: FLEXIBLE
    },
    {
      icon: productFFLMP,
      title: 'Flexible home loan.',
      subtitle: 'With members package',
      background: '#f5fdef',
      value: FLEXIBLEMP
    }
  ]
};
