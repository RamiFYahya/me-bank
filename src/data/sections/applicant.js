import singleCup from '../../assets/icons/single-cup.svg';
import doubleCup from '../../assets/icons/double-cup.svg';

export default {
  intro: {
    title: `Who is applying?`,
    subtitle: `This is where you  tell us if it’s a solo  or joint application.`,
    background: '#f7f7f7'
  },
  options: [
    {
      icon: singleCup,
      title: 'Just me.',
      subtitle: `I'm going it alone.`,
      background: '#e1f2fb',
      value: 'single',
      onSaveTitle: `It's just me.`
    },
    {
      icon: doubleCup,
      title: 'Two of us.',
      subtitle: `We're doing it together.`,
      background: '#f5fdef',
      value: 'joint',
      onSaveTitle: `It's the two of us.`
    }
  ]
};
