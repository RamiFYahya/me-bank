export default {
  intro: {
    title: `Repayment  details.`,
    subtitle: `How much you'll need to cover your loan repayments.`
  },
  background: `#1d1d1d`
}
