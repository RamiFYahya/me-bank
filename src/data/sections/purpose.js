import { PARAMETERS } from '../../constants';

export default (prefix) => ({
  intro: {
    title: `What's the purpose?`,
    subtitle: `We have different loan types for different needs,  so tell us why.`,
    background: '#f7f7f7'
  },
  options: [
    {
      title: `${prefix} buying.`,
      subtitle: `Lorem ipsum dolor sit amet.`,
      background: '#fff8ee',
      value: PARAMETERS.BUYING
    },
    {
      title: `${prefix} refinancing.`,
      subtitle: `Lorem ipsum dolor sit amet.`,
      background: '#ede4da',
      value: PARAMETERS.REFINANCE
    }
  ]
});
