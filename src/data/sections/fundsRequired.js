export default {
  intro: {
    title: `Funds required.`,
    subtitle: `This is how much you need to cover your loan.`
  },
  background: '#daeff9'
}
