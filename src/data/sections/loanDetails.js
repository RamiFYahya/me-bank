export default {
  intro: {
    title: `Loan details.`,
    subtitle: `What you're borrowing.`
  },
  background: `#f4f4f4`
}
