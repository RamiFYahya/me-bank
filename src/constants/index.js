export const SLIDER = {
  MIN: 1,
  MAX: 30
}

export const PARAMETERS = {
  SINGLE: 'single',
  JOINT: 'joint',
  BUYING: 'buying',
  REFINANCE: 'refinance',
  PERSONAL: 'personal',
  PERSONAL_NEW_HOME: 'personal new home',
  INVESTMENT: 'investment',
  INVESTMENT_FIRST_HOME: 'first home investment',
  ESTABLISHED_HOME: 'established home',
  NEWLY_CONSTRUCTED: 'newly constructed',
  VACANT_LAND: 'vacant land'
}

export const STEPS = {
  APPLICANT: 'applicant',
  PURPOSE: 'purpose',
  PROPERTY: 'property',
  FINANCES: 'finances',
  BORROW: 'borrow'
}
