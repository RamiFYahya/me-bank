import React from 'react'

export const ERRORS = {
  SHOW: {
    touched: true,
    focus: false
  },
  WRAPPER: (props) => <div className="errors">{props.children}</div>,
  COMPONENT: (props) => <div className="error">{props.children}</div>
}

export const CONTROL = {
  MAP_PROPS: {
    className: ({fieldValue}) => fieldValue.touched && !fieldValue.valid && !fieldValue.focus ? 'Control-input Control-error' : 'Control-input'
  }
}
