import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { lightBlue500 } from 'material-ui/styles/colors';
import spacing from './spacing';
import palette from './palette'

const theme = {
  spacing: spacing,
  fontFamily: '"Gotham SSm A", "Helvetica Neue", Arial, Helvetica, sans-serif',
  palette: palette,
  textField: {
    textColor: lightBlue500
  }
};

const muiTheme = getMuiTheme(theme);

export default muiTheme;
