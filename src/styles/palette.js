import {
  blue100, lightBlue500, blue700,
  red500, redA200,
  grey500,
  white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';

const palette = {
  primary1Color: lightBlue500,
  primary2Color: blue700,
  primary3Color: blue100,
  accent1Color: red500,
  accent2Color: redA200,
  accent3Color: grey500,
  textColor: darkBlack,
  alternateTextColor: white,
  canvasColor: white,
  borderColor: fade(lightBlue500, 0.7),
  disabledColor: fade(darkBlack, 0.3),
  pickerHeaderColor: lightBlue500,
  clockCircleColor: fade(darkBlack, 0.07),
  shadowColor: fullBlack
};

export default palette;
