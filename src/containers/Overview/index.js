import React from 'react';
import { Col } from 'react-flexbox-grid';
import ProductSelector from './ProductSelector';
import ProductDetails from './ProductDetails';
import LoanDetails from './LoanDetails';
import FundsRequired from './FundsRequired';
import RepaymentDetails from './RepaymentDetails';

const Overview = () => (
  <div>
    <ProductSelector />
    <Col mdOffset={3} md={8}>
      <ProductDetails />
    </Col>
    <LoanDetails />
    <FundsRequired />
    <div style={{marginTop: '2rem'}}>
      <RepaymentDetails />
    </div>
  </div>
)

export default Overview;
