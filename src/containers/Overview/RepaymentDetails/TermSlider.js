import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import Slider from '../../../components/Slider';

const mapDispatchToProps = {
  onSliderChange: actions.change
}

const mapStateToProps = (state, ownProps) => {
  return {
    term: state.loan.mortgage.term,
    model: 'loan.mortgage.term'
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Slider)
