import { connect } from 'react-redux'
import Decimal from 'decimal.js'
import { convertToMonthly, calculateMonthlyRepayment } from '../utilities/calculators'
import IncomeExpensesChart from '../components/IncomeExpensesChart'

const mapStateToProps = (state, ownProps) => {
  const {
    finances
  } = state.loan;

  const expenses = convertToMonthly(finances.expensesFrequency, finances.expenses);
  const income = convertToMonthly(finances.incomeFrequency, finances.income);
  const repayment = ownProps.repayment;
  const balance = Decimal.sub(income, Decimal.add(expenses, repayment));

  return {
    expenses,
    income,
    repayment,
    balance
  }
}

const ConnectedIncomeExpensesChart = connect(
  mapStateToProps
)(IncomeExpensesChart)

export default ConnectedIncomeExpensesChart
