import React from 'react';
import { connect } from 'react-redux';
import { Form, Control } from 'react-redux-form';
import Select from '../../../components/Select';
import { currencyFormat } from '../../../utilities/formatter';
import { WEEKLY, FORTNIGHTLY, MONTHLY } from '../../../constants/frequency';
import { CONTROL } from '../../../constants/formOptions';

const RepaymentFrequency = ({ frequency, paymentPerPeriod }) => (
  <Form
    model="loan.mortgage"
  >
    <Select>
      <Control.select
        model=".frequency"
        mapProps={CONTROL.MAP_PROPS}
      >
        <option value={WEEKLY}>{WEEKLY}</option>
        <option value={FORTNIGHTLY}>{FORTNIGHTLY}</option>
        <option value={MONTHLY}>{MONTHLY}</option>
      </Control.select>
    </Select>
    <div className="u-repayment-frequency">
      <h4>loan repayment</h4>
      <h1>{currencyFormat(paymentPerPeriod)}</h1>
    </div>
  </Form>
)

const mapStateToProps = (state, ownProps) => {
  return {
    frequency: state.loan.mortgage.frequency,
    paymentPerPeriod: ownProps.payment
  }
}

export default connect(mapStateToProps)(RepaymentFrequency)
