import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-flexbox-grid';
import { Form, Control } from 'react-redux-form';
import times from 'lodash/times';
import Radio from '../../../components/Radio';
import ControlWrapper from '../../../components/ControlWrapper';
import Select from '../../../components/Select';
import { PARAMETERS } from '../../../constants';
import { CONTROL } from '../../../constants/formOptions';

const RepaymentType = ({ interestOnlyAvailable, interestOnlyActive }) => (
  interestOnlyAvailable &&
  <div style={{marginBottom: interestOnlyActive ? '0' : '5.5rem'}}>
    <Row top="xs">
      <Col xs={12}>
        <div className="h6" style={{marginBottom: '1rem'}}>
          Which type of repayments do you want to take up?
        </div>
      </Col>
      <Col xs={12}>
        <Form
          model="loan.repayment"
        >
          <Row start="xs">
            <Col xs={12}>
              <Row start="xs">
                <Col xs={3}>
                  <Control.radio
                    model=".interestOnly"
                    value={false}
                    component={Radio}
                    label="Principal and Interest"
                  />
                </Col>
                <Col xs={9}>
                  <Row start="xs">
                    <Col xs={3}>
                      <Control.radio
                        model=".interestOnly"
                        value={true}
                        component={Radio}
                        label="Interest only"
                      />
                    </Col>
                    <Col xs={4}>
                      {
                        interestOnlyActive &&
                        <ControlWrapper
                          label={{
                            position: 'pre',
                            text: `Duration`
                          }}
                          controlStyle="stacked"
                        >
                          <Select>
                            <Control.select
                              model=".interestOnlyDuration"
                              mapProps={CONTROL.MAP_PROPS}
                            >
                              { times(5, index => <option value={index + 1} key={index + 1}>{index + 1 === 1 ? `${index + 1} year` : `${index + 1} years`}</option>) }
                            </Control.select>
                          </Select>
                        </ControlWrapper>
                      }
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  </div>
)

const mapStateToProps = (state) => {
  return {
    interestOnlyAvailable: state.loan.property.purpose === PARAMETERS.INVESTMENT,
    interestOnlyActive: state.loan.repayment.interestOnly
  }
}

export default connect(mapStateToProps)(RepaymentType);
