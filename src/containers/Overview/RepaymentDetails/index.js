import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-flexbox-grid';
import accounting from 'accounting';
import omit from 'lodash/omit';
import assignWith from 'lodash/assignWith';
import RepaymentType from './RepaymentType';
import AmortizationChart from '../../../components/AmortizationChart'
import RepaymentFrequency from './RepaymentFrequency';
import TermSlider from './TermSlider';
import Section from '../../../components/Section';
import sectionContent from '../../../data/sections/repaymentDetails';
import { amountOwingSchedule } from '../../../utilities/calculators/amortization';
import coerceToNumber from '../../../utilities/calculators/coerceToNumber';
import { convertToCents } from '../../../utilities/calculators';

const customizer = (objValue, srcValue, key, object, source) => {
  return key === 'fixedAmount' ? accounting.unformat(srcValue) : srcValue
}

const computeAmortizationFromState = (state) => {
  const principal = convertToCents(accounting.unformat(state.loan.borrow.amount));
  const term = state.loan.mortgage.term;
  const interestRate = state.mortgage.interest.variableRate;
  const frequency = state.loan.mortgage.frequency;
  const omitVariableRate = omit(state.mortgage.interest, 'variableRate');
  const split = assignWith({}, state.loan.split, omitVariableRate, customizer);
  const repayment = state.loan.repayment;
  return amountOwingSchedule(principal, term, interestRate, frequency, split, repayment);
}

const RepaymentDetails = ({ sectionContent, repaymentData }) => (
  <Section sectionContent={sectionContent} inverted={true}>
    <div style={{width: '100%'}}>
      <RepaymentType />
      <Row>
        <Col xs={5}>
          <RepaymentFrequency payment={repaymentData.payment.amount.toNumber()}/>
          <TermSlider />
        </Col>
        <Col xs={5}>
          <AmortizationChart schedule={coerceToNumber(repaymentData.schedule)} />
        </Col>
      </Row>
    </div>
  </Section>
);

const mapStateToProps = (state) => {
  return {
    sectionContent,
    repaymentData: computeAmortizationFromState(state)
  }
}

export default connect(mapStateToProps)(RepaymentDetails);
