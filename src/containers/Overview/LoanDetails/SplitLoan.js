import React from 'react';
import { connect } from 'react-redux';
import validator from 'validator';
import { Row, Col } from 'react-flexbox-grid';
import { Form, Control, Errors } from 'react-redux-form';
import accounting from 'accounting';
import { formatNumber } from '../../../utilities/formatter';
import times from 'lodash/times';
import ControlWrapper from '../../../components/ControlWrapper';
import Select from '../../../components/Select';
import InterestRate from '../../../components/InterestRate';
import { ERRORS, CONTROL } from '../../../constants/formOptions';
import { BASIC } from '../../../constants/productCodes';

const SplitLoan = ({ splitActive, fixedRate }) => (
  splitActive &&
  <Form
    model="loan.split"
  >
    <Row>
      <Col xs={6}>
        <ControlWrapper
          label={{
            position: 'pre',
            text: `Fixed amount`
          }}
          controlType="currency"
          controlStyle="stacked"
        >
          <Control.text
            model=".fixedAmount"
            mapProps={CONTROL.MAP_PROPS}
            parser={(val) => formatNumber(val)}
            validators={{
              isRequired: (val) => !validator.isEmpty(val),
              isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`)
            }}
          />
          <Errors
            model=".fixedAmount"
            show={ERRORS.SHOW}
            wrapper={ERRORS.WRAPPER}
            component={ERRORS.COMPONENT}
            messages={{
              isRequired: 'amount is required',
              isNumeric: 'amount should be numeric'
            }}
          />
        </ControlWrapper>
      </Col>

      <Col xs={6}>
        <Row>
          <Col xs={4}>
            <ControlWrapper
              label={{
                position: 'pre',
                text: `Duration`
              }}
              controlStyle="stacked"
            >
              <Select>
                <Control.select
                  model=".fixedDuration"
                  mapProps={CONTROL.MAP_PROPS}
                >
                  { times(7, index => <option value={index + 1} key={index + 1}>{index + 1 === 1 ? `${index + 1} year` : `${index + 1} years`}</option>) }
                </Control.select>
              </Select>
            </ControlWrapper>
          </Col>
          <Col xs={8}>
            <InterestRate rate={fixedRate} prefix="@" fontSize="5rem"/>
          </Col>
        </Row>
      </Col>
    </Row>
  </Form>
)

const mapStateToProps = (state) => {
  return {
    splitActive: state.loan.split.active && state.loan.mortgage.activeProduct !== BASIC,
    fixedRate: state.mortgage.interest.fixedRate
  }
}

export default connect(mapStateToProps)(SplitLoan);
