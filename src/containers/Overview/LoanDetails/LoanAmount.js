import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-flexbox-grid';
import { Form, Control, Errors } from 'react-redux-form';
import validator from 'validator';
import accounting from 'accounting';
import { formatNumber } from '../../../utilities/formatter';
import ControlWrapper from '../../../components/ControlWrapper';
import { ERRORS, CONTROL } from '../../../constants/formOptions';

const LoanDetailsForm = () => (
  <Form
    model="loan"
  >
    <Row>
      <Col xs={6}>
        <ControlWrapper
          label={{
            position: 'pre',
            text: `Property Value`
          }}
          controlType="currency"
          controlStyle="stacked"
        >
          <Control.text
            model="loan.property.value"
            parser={(val) => formatNumber(val)}
            mapProps={CONTROL.MAP_PROPS}
            validators={{
              isRequired: (val) => !validator.isEmpty(val),
              isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
              isBetween: (val) => accounting.unformat(val) >= 100000 && accounting.unformat(val) <= 10000000
            }}
          />
          <Errors
            model="loan.property.value"
            show={ERRORS.SHOW}
            wrapper={ERRORS.WRAPPER}
            component={ERRORS.COMPONENT}
            messages={{
              isRequired: 'amount is required',
              isNumeric: 'amount should be numeric',
              isBetween: 'between $100,000 - $10,000,000'
            }}
          />
        </ControlWrapper>
      </Col>
      <Col xs={6}>
        <ControlWrapper
          label={{
            position: 'pre',
            text: `I want to borrow`
          }}
          controlType="currency"
          controlStyle="stacked"
        >
          <Control.text
            model="loan.borrow.amount"
            parser={(val) => formatNumber(val)}
            mapProps={CONTROL.MAP_PROPS}
            validators={{
              isRequired: (val) => !validator.isEmpty(val),
              isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
              isBetween: (val) => accounting.unformat(val) >= 100000 && accounting.unformat(val) <= 10000000
            }}
          />
          <Errors
            model="loan.borrow.amount"
            show={ERRORS.SHOW}
            wrapper={ERRORS.WRAPPER}
            component={ERRORS.COMPONENT}
            messages={{
              isRequired: 'amount is required',
              isNumeric: 'amount should be numeric',
              isBetween: 'between $100,000 and $10,000,000'
            }}
          />
        </ControlWrapper>
      </Col>
    </Row>
  </Form>
)

export default connect()(LoanDetailsForm);
