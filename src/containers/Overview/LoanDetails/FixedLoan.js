import React from 'react';
import { connect } from 'react-redux';
import { Form, Control } from 'react-redux-form';
import { Row, Col } from 'react-flexbox-grid';
import Checkbox from '../../../components/Checkbox';
import { BASIC } from '../../../constants/productCodes';

const FixedLoan = ({ fixedLoanAvailable }) => (
  fixedLoanAvailable &&
  <Form
    model="loan.split"
  >
    <Row>
      <Col xs={12} style={{marginBottom: '2rem'}}>
        <Control.checkbox
          model=".active"
          component={Checkbox}
          controlProps={{
            label: 'Fix your rate',
            size: 'large'
          }}
        />
      </Col>
    </Row>
  </Form>
)

const mapStateToProps = (state) => {
  return {
    fixedLoanAvailable: state.loan.mortgage.activeProduct !== BASIC
  }
}

export default connect(mapStateToProps)(FixedLoan);
