import React from 'react';
import { connect } from 'react-redux';
import Section from '../../../components/Section';
import sectionContent from '../../../data/sections/loanDetails';
import LoanAmount from './LoanAmount';
import FixedLoan from './FixedLoan';
import SplitLoan from './SplitLoan';

const LoanDetails = ({ splitAvailable }) => (
  <Section sectionContent={sectionContent}>
    <div>
      <LoanAmount />
      <FixedLoan />
      <SplitLoan />
    </div>
  </Section>
);

const mapStateToProps = (state) => {
  return {
    sectionContent
  }
}

export default connect(mapStateToProps)(LoanDetails);
