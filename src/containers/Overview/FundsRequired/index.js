import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-flexbox-grid';
import Section from '../../../components/Section';
import BreakdownTable from '../../../components/BreakdownTable';
import { currencyFormat } from '../../../utilities/formatter';
import sectionContent from '../../../data/sections/fundsRequired';

const FundsRequired = ({ sectionContent, funds, deposit, costs, refund }) => (
  <Section sectionContent={sectionContent}>
    <Col xs={12} className="u-no-padding">
      <Row top="xs">
        <Col xs={4} className="u-no-padding">
          <h1
            style={{
              marginTop: '0',
              lineHeight: '1'
            }}
          >
            {currencyFormat(funds, false)}
          </h1>
        </Col>
        <Col xs={5}>
          <BreakdownTable deposit={deposit} costs={costs} refund={refund} />
        </Col>
      </Row>
    </Col>
  </Section>
);

// Hardcoded state passed in for UI purposes - values should be calculated internally or coming from external API services
const mapStateToProps = (state) => {
  return {
    sectionContent,
    funds: '148500',
    deposit: {
      amount: '100000',
      label: 'Deposit'
    },
    costs: [
      {
        amount: '20000',
        label: 'Stamp duty'
      },
      {
        amount: '1500',
        label: 'Mortgage registration'
      },
      {
        amount: '500',
        label: 'Registration of transfer of land'
      },
      {
        amount: '6500',
        label: 'Lenders Mortgage Insurance'
      }
    ],
    refund: {
      amount: '10000',
      label: 'First home buyers grant'
    }
  }
}

export default connect(mapStateToProps)(FundsRequired);
