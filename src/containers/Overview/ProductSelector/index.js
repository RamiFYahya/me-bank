import React from 'react';
import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import MultipleChoice from '../../../components/MultipleChoice';
import sectionContent from '../../../data/sections/products';

const ProductSelector = ({ content, model, onChange, activeProduct }) => (
  <MultipleChoice
    content={content}
    model={model}
    onChange={onChange}
    selectedOption={activeProduct}
  />
)

const mapStateToProps = (state) => {
  return {
    content: sectionContent,
    model: 'loan.mortgage.activeProduct',
    activeProduct: state.loan.mortgage.activeProduct
  }
}

const mapDispatchToProps = {
  onChange: actions.change
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductSelector)
