import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-flexbox-grid';
import ProductDetails from '../../../components/ProductDetails';
import InterestRate from '../../../components/InterestRate';
import products from '../../../data/products';

const ProductDetailsConnected = ({ product, variableRate }) => (
  <Row className="u-row-padding">
    <Col xs={12} sm={4}>
      <p style={{marginBottom: '0'}}>This product has an interest rate of</p>
      <InterestRate rate={variableRate} fontSize="10rem" />
    </Col>
    <Col xs={12} sm={8}>
      <ProductDetails product={product} />
    </Col>
  </Row>
)

const mapStateToProps = (state) => {
  return {
    product: products[state.loan.mortgage.activeProduct],
    variableRate: state.mortgage.interest.variableRate
  }
}

export default connect(mapStateToProps)(ProductDetailsConnected)
