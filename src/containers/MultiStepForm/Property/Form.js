import React from 'react';
import { connect } from 'react-redux';
import { Form, Control, Errors } from 'react-redux-form';
import validator from 'validator';
import accounting from 'accounting';
import { formatNumber } from '../../../utilities/formatter';
import ControlWrapper from '../../../components/ControlWrapper';
import Select from '../../../components/Select';
import Button from '../../../components/Button';
import { ERRORS, CONTROL } from '../../../constants/formOptions';
import { PARAMETERS } from '../../../constants';
import { activateStep, completeStep } from '../../../actions';

const PropertyForm = ({ activateStep, completeStep, applicantType, stepId, nextStepId }) => (
  <Form
    model="loan.property"
    validators={{
      value: {
        isRequired: (val) => !validator.isEmpty(val),
        isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
        isBetween: (val) => accounting.unformat(val) >= 100000 && accounting.unformat(val) <= 10000000
      },
      purpose: {
        isRequired: (val) => !validator.isEmpty(val)
      },
      state: {
        isRequired: (val) => !validator.isEmpty(val)
      }
    }}
    onSubmit={() => {
      completeStep(stepId)
      activateStep(nextStepId)
    }}
    className="Form-wrapper"
  >

    <ControlWrapper
      label={{
        position: 'pre',
        text: `The property is worth`
      }}
      controlType="currency"
    >
      <Control.text
        model=".value"
        parser={(val) => formatNumber(val)}
        mapProps={CONTROL.MAP_PROPS}
      />
      <Errors
        model=".value"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'amount is required',
          isNumeric: 'amount should be numeric',
          isBetween: 'only $100,000 - $10,000,000'
        }}
      />
    </ControlWrapper>

    <ControlWrapper
      label={{
        position: 'pre',
        text: `and is going to be`
      }}
    >
      <Select>
        <Control.select
          model=".purpose"
          mapProps={CONTROL.MAP_PROPS}
        >
          <option value={PARAMETERS.PERSONAL}>{applicantType === PARAMETERS.JOINT ? 'our' : 'my' } first home to live in</option>
          <option value={PARAMETERS.INVESTMENT}>an investment property</option>
          <option value={PARAMETERS.INVESTMENT_FIRST_HOME}>{applicantType === PARAMETERS.JOINT ? 'our' : 'my' } first home as an investment property</option>
          <option value={PARAMETERS.PERSONAL_NEW_HOME}>a new place for {applicantType === PARAMETERS.JOINT ? 'us' : 'me' } to live in</option>
        </Control.select>
      </Select>
      <Errors
        model=".purpose"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'Property type is required',
        }}
      />
    </ControlWrapper>

    <ControlWrapper
      label={{
        position: 'pre',
        text: `and it's`
      }}
    >
      <Select>
        <Control.select
          model=".description"
          mapProps={CONTROL.MAP_PROPS}
        >
          <option value={PARAMETERS.ESTABLISHED_HOME}>an established home</option>
          <option value={PARAMETERS.NEWLY_CONSTRUCTED}>a newly constructed home</option>
          <option value={PARAMETERS.VACANT_LAND}>vacant land to build a home</option>
        </Control.select>
      </Select>
      <Errors
        model=".description"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'Property description is required',
        }}
      />
    </ControlWrapper>

    <ControlWrapper
      label={{
        position: 'pre',
        text: `located in`
      }}
    >
      <Select>
        <Control.select
          model=".state"
          mapProps={CONTROL.MAP_PROPS}
        >
          <option value="ACT">ACT</option>
          <option value="NSW">NSW</option>
          <option value="VIC">VIC</option>
          <option value="QLD">QLD</option>
          <option value="SA">SA</option>
          <option value="WA">WA</option>
          <option value="TAS">TAS</option>
          <option value="NT">NT</option>
        </Control.select>
      </Select>
      <Errors
        model=".state"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'Location is required'
        }}
      />
    </ControlWrapper>

    <div className="Form-submit">
      <Button type="submit" kind="primary">Next</Button>
    </div>
  </Form>
);


const mapStateToProps = (state) => {
  return {
    applicantType: state.loan.type.applicant,
    nextStepId: state.steps.finances.id
  }
}

const mapDispatchToProps = {
  activateStep,
  completeStep
}

export default connect(mapStateToProps, mapDispatchToProps)(PropertyForm);
