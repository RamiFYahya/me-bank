import React from 'react';
import { connect } from 'react-redux';
import { editStep } from '../../../actions';
import Section from '../../../components/Section';
import Step from '../../../components/Step';
import StepSummary from '../../../components/StepSummary';
import StepWrapper from '../../../components/StepWrapper';
import Form from './Form';
import sectionContent from '../../../data/sections/property';
import summaryGenerator from '../../../utilities/summaryGenerator';

const Property = ({ step, editStep, stepSummary, sectionContent }) => (
  <StepWrapper inverted={true}>
    {
      !step.completed ?
        <Step active={step.active} >
          <Section sectionContent={sectionContent} >
            <Form stepId={step.id}/>
          </Section>
        </Step>
      :
        <StepSummary
          title={stepSummary}
          background={sectionContent.background}
          editStep={editStep}
          stepId={step.id}
        />
    }
  </StepWrapper>
)

const mapStateToProps = (state) => {
  const step = state.steps.property;
  return {
    stepSummary: summaryGenerator(state, step.id),
    step,
    sectionContent
  }
}

const mapDispatchToProps = {
  editStep
}

export default connect(mapStateToProps, mapDispatchToProps)(Property);
