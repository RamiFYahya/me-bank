import React from 'react';
import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import { activateStep, completeStep, editStep } from '../../../actions';
import Step from '../../../components/Step';
import StepWrapper from '../../../components/StepWrapper';
import StepSummary from '../../../components/StepSummary';
import MultipleChoice from '../../../components/MultipleChoice';
import { PARAMETERS } from '../../../constants';
import sectionContent from '../../../data/sections/purpose';
import summaryGenerator from '../../../utilities/summaryGenerator';

const Purpose = ({ content, step, nextStepId, model, onChange, activateStep, completeStep, editStep, stepSummary, summaryBackground, applicationType }) => {
  return (
    <StepWrapper>
      {
        !step.completed ?
          <Step active={step.active} >
            <MultipleChoice
              content={content}
              model={model}
              onChange={onChange}
              completeStep={completeStep}
              activateStep={activateStep}
              stepId={step.id}
              nextStepId={nextStepId}
              selectedOption={applicationType}
            />
          </Step>
        :
          <StepSummary
            title={stepSummary}
            background={summaryBackground}
            editStep={editStep}
            stepId={step.id}
          />
      }
    </StepWrapper>
  )
};

const mapStateToProps = (state) => {
  const {
    type
  } = state.loan;

  const prefix = type.applicant === PARAMETERS.JOINT ? `We're` : `I'm`;
  const content = sectionContent(prefix);
  const summaryBackground = type.application === PARAMETERS.BUYING ? content.options[0].background : content.options[1].background;
  const step = state.steps.purpose;
  return {
    content,
    step,
    summaryBackground,
    model: 'loan.type.application',
    stepSummary: summaryGenerator(state, step.id),
    nextStepId: state.steps.property.id,
    applicationType: state.loan.type.application
  }
}

const mapDispatchToProps = {
  onChange: actions.change,
  completeStep,
  activateStep,
  editStep
}

export default connect(mapStateToProps, mapDispatchToProps)(Purpose);
