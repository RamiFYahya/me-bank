import React from 'react';
import { connect } from 'react-redux';
import { Form, Control, Errors } from 'react-redux-form';
import validator from 'validator';
import accounting from 'accounting';
import { formatNumber } from '../../../utilities/formatter';
import ControlWrapper from '../../../components/ControlWrapper';
import { completeStep } from '../../../actions';
import { ERRORS, CONTROL } from '../../../constants/formOptions';
import { PARAMETERS } from '../../../constants';

const BorrowForm = ({ applicantType, stepId }) => (
  <Form
    model="loan.borrow"
    validators={{
      amount: {
        isRequired: (val) => !validator.isEmpty(val),
        isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
        isBetween: (val) => accounting.unformat(val) >= 100000 && accounting.unformat(val) <= 10000000
      }
    }}
    className="Form-wrapper"
  >
    <ControlWrapper
      label={{
        position: 'pre',
        text: `${applicantType === PARAMETERS.JOINT ? `We'd` : `I'd`} like to borrow`
      }}
      controlType="currency"
    >
      <Control.text
        model=".amount"
        parser={(val) => formatNumber(val)}
        mapProps={CONTROL.MAP_PROPS}
      />
      <Errors
        model=".amount"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'amount is required',
          isNumeric: 'amount should be numeric',
          isBetween: 'between $100,000 and $10,000,000'
        }}
      />
    </ControlWrapper>
  </Form>
)

const mapStateToProps = (state) => {
  return {
    applicantType: state.loan.type.applicant
  }
}

const mapDispatchToProps = {
  completeStep
}

export default connect(mapStateToProps, mapDispatchToProps)(BorrowForm);
