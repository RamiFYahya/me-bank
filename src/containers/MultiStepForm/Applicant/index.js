import React from 'react';
import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import { activateStep, completeStep, editStep } from '../../../actions';
import Step from '../../../components/Step';
import StepSummary from '../../../components/StepSummary';
import StepWrapper from '../../../components/StepWrapper';
import MultipleChoice from '../../../components/MultipleChoice';
import { PARAMETERS } from '../../../constants';
import sectionContent from '../../../data/sections/applicant';
import summaryGenerator from '../../../utilities/summaryGenerator';

const Applicant = ({ content, step, nextStepId, model, onChange, activateStep, completeStep, editStep, stepSummary, summaryBackground, applicantType  }) => (
  <StepWrapper>
    {
      !step.completed ?
        <Step active={step.active} >
          <MultipleChoice
            content={content}
            model={model}
            onChange={onChange}
            completeStep={completeStep}
            activateStep={activateStep}
            stepId={step.id}
            nextStepId={nextStepId}
            selectedOption={applicantType}
          />
        </Step>
      :
        <StepSummary
          title={stepSummary}
          background={summaryBackground}
          editStep={editStep}
          stepId={step.id}
        />
    }
  </StepWrapper>
);

const mapStateToProps = (state) => {
  const summaryBackground = state.loan.type.applicant === PARAMETERS.SINGLE ? sectionContent.options[0].background : sectionContent.options[1].background;
  const step = state.steps.applicant;
  return {
    step,
    summaryBackground,
    content: sectionContent,
    model: 'loan.type.applicant',
    stepSummary: summaryGenerator(state, step.id),
    nextStepId: state.steps.purpose.id,
    applicantType: state.loan.type.applicant
  }
}

const mapDispatchToProps = {
  onChange: actions.change,
  completeStep,
  activateStep,
  editStep
}

export default connect(mapStateToProps, mapDispatchToProps)(Applicant);
