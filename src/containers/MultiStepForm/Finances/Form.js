import React from 'react';
import { connect } from 'react-redux';
import { Form, Control, Errors } from 'react-redux-form';
import validator from 'validator';
import accounting from 'accounting';
import { formatNumber } from '../../../utilities/formatter';
import ControlWrapper from '../../../components/ControlWrapper';
import Select from '../../../components/Select';
import Button from '../../../components/Button';
import { ERRORS, CONTROL } from '../../../constants/formOptions';
import { PARAMETERS } from '../../../constants';
import { WEEKLY, FORTNIGHTLY, MONTHLY, YEARLY } from '../../../constants/frequency'
import { WEEK, FORTNIGHT, MONTH, YEAR } from '../../../constants/frequency'
import { activateStep, completeStep } from '../../../actions';

const FinancesForm = ({ applicantType, activateStep, completeStep, stepId, nextStepId }) => (
  <Form
    model="loan.finances"
    validators={{
      'incomeFrequency': {
        isRequired: (val) => !validator.isEmpty(val)
      },
      'income': {
        isRequired: (val) => !validator.isEmpty(val),
        isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
        isBetween: (val) => accounting.unformat(val) >= 500 && accounting.unformat(val) <= 1000000
      },
      'expensesFrequency': {
        isRequired: (val) => !validator.isEmpty(val)
      },
      'expenses': {
        isRequired: (val) => !validator.isEmpty(val),
        isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
        isBetween: (val) => accounting.unformat(val) >= 500 && accounting.unformat(val) <= 1000000
      }
    }}
    onSubmit={() => {
      completeStep(stepId)
      activateStep(nextStepId)
    }}
    className="Form-wrapper"
  >

    <ControlWrapper
      label={{
        position: 'pre',
        text: `${applicantType === PARAMETERS.JOINT ? 'Our ' : 'My '}`
      }}
    >
      <Select>
        <Control.select
          model=".incomeFrequency"
          mapProps={CONTROL.MAP_PROPS}
        >
          <option value={WEEKLY}>weekly</option>
          <option value={FORTNIGHTLY}>fortnightly</option>
          <option value={MONTHLY}>monthly</option>
          <option value={YEARLY}>yearly</option>
        </Control.select>
      </Select>
      <Errors
        model=".incomeFrequency"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'income frequency is required',
        }}
      />
      <label className="Control-label"> gross income</label>
    </ControlWrapper>


    <ControlWrapper
      label={{
        position: 'pre',
        text: `is around`
      }}
      controlType="currency"
    >
      <Control.text
        model=".income"
        parser={(val) => formatNumber(val)}
        mapProps={CONTROL.MAP_PROPS}
      />
      <Errors
        model=".income"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'income amount is required',
          isNumeric: 'income amount should be numeric',
          isBetween: 'between $500 and $1,000,000'
        }}
      />
    </ControlWrapper>

    <ControlWrapper
      label={{
        position: 'pre',
        text: `with expenses of about`
      }}
      controlType="currency"
    >
      <Control.text
        model=".expenses"
        parser={(val) => formatNumber(val)}
        mapProps={CONTROL.MAP_PROPS}
      />
      <Errors
        model=".expenses"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'expenses amount is required',
          isNumeric: 'expenses amount should be numeric',
          isBetween: 'between $500 and $1,000,000'
        }}
      />
    </ControlWrapper>

    <ControlWrapper
      label={{
        position: 'pre',
        text: `every`
      }}
    >
      <Select>
        <Control.select
          model=".expensesFrequency"
          mapProps={CONTROL.MAP_PROPS}
        >
          <option value={WEEK}>week</option>
          <option value={FORTNIGHT}>fortnight</option>
          <option value={MONTH}>month</option>
          <option value={YEAR}>year</option>
        </Control.select>
      </Select>
      <Errors
        model=".expenses.frequency"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'field required',
        }}
      />
    </ControlWrapper>

    <div className="Form-submit">
      <Button type="submit" kind="primary">Next</Button>
    </div>
  </Form>
);

const mapStateToProps = (state) => {
  return {
    applicantType: state.loan.type.applicant,
    nextStepId: state.steps.borrow.id
  }
}


const mapDispatchToProps = {
  activateStep,
  completeStep
}

export default connect(mapStateToProps, mapDispatchToProps)(FinancesForm);
