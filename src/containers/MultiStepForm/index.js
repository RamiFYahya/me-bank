import React from 'react';
import { connect } from 'react-redux';
import Step from '../../components/Step';
import Button from '../../components/Button';
import Applicant from './Applicant';
import Purpose from './Purpose';
import Property from './Property';
import Finances from './Finances';
import Borrow from './Borrow';

const Submit = ({ active }) => (
  <Step active={active}>
    <div className="l-container" style={{textAlign: 'right'}}>
      <Button kind="secondary" size="large">next</Button>
    </div>
  </Step>
)

const MultiStepForm = ({ formsValid }) => (
  <div>
    <Applicant />
    <Purpose />
    <Property />
    <Finances />
    <Borrow />
    <Submit active={formsValid} />
  </div>
)

const mapStateToProps = (state) => {
  return {
    formsValid: state.loan.forms.$form.valid
  }
}

export default connect(mapStateToProps)(MultiStepForm)
