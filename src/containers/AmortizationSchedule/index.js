import React from 'react';
import { connect } from 'react-redux';
import S from 'string';
import AmortizationTable from '../../components/AmortizationTable';
import Header from '../../components/Header';
import { amountOwingSchedule, yearlyAmortizationSchedule, generateAmortizationSchedule } from '../../utilities/calculators/amortization';
import { convertToCents } from '../../utilities/calculators';
import AmortizationForm from './Form';

const AmortizationSchedule = ({ principal, term, frequency, mergedSplit, fixedRate, variableRate, repayment }) => (
  <div>
    <Header title="Amortization Calculators" subtitle={`Fixed Rate@${fixedRate}%  Variable Rate@${variableRate}%`}/>
    <AmortizationForm />

    <Header title="Amount Owing Schedule" />
    <AmortizationTable data={amountOwingSchedule(principal, term, variableRate, frequency, mergedSplit, repayment).schedule}/>

    <Header title="Yearly Amortization Schedule" />
    <AmortizationTable data={yearlyAmortizationSchedule(principal, term, variableRate, frequency, mergedSplit, repayment).schedule}/>

    <Header title={`${S(frequency).titleCase().s} Amortization Schedule`} />
    <AmortizationTable data={generateAmortizationSchedule(principal, term, variableRate, frequency, mergedSplit, repayment).schedule}/>
  </div>
)


const mapStateToProps = (state) => {
  const {
    mortgage: {
      term,
      frequency,
    },
    borrow: {
      amount
    },
    split,
    repayment
  } = state.loan;

  const {
    fixedRate,
    variableRate
  } = state.mortgage.interest;

  const principal = convertToCents(amount);
  const mergedSplit = Object.assign({}, state.mortgage.interest, split);

  return {
    principal,
    term,
    frequency,
    mergedSplit,
    fixedRate,
    variableRate,
    repayment
  }
}

export default connect(mapStateToProps)(AmortizationSchedule);
