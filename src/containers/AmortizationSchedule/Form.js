import React from 'react';
import { connect } from 'react-redux';
import { Form, Control, Errors } from 'react-redux-form';
import validator from 'validator';
import accounting from 'accounting';
import S from 'string';
import ControlWrapper from '../../components/ControlWrapper';
import Select from '../../components/Select';
import { ERRORS, CONTROL } from '../../constants/formOptions';
import { WEEKLY, FORTNIGHTLY, MONTHLY } from '../../constants/frequency'
import times from 'lodash/times';

const AmortizationFormFixed = () => (
  <div className="u-amortization-form">
    <ControlWrapper
      label={{
        position: 'pre',
        text: `Split Loan Amount`
      }}
      controlType="currency"
      controlStyle="stacked"
    >
      <Control.text
        model="loan.split.fixedAmount"
        validators={{
          isRequired: (val) => !validator.isEmpty(val),
          isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
          isBetween: (val) => accounting.unformat(val) >= 100000 && accounting.unformat(val) <= 10000000
        }}
        mapProps={CONTROL.MAP_PROPS}
        updateOn={['blur']}
      />
      <Errors
        model="loan.split.fixedAmount"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'amount is required',
          isNumeric: 'amount should be numeric',
          isBetween: 'between $100,000 and $10,000,000'
        }}
      />
    </ControlWrapper>

    <ControlWrapper
        label={{
          position: 'pre',
          text: `Fixed Duration`
        }}
        controlStyle="stacked"
      >
        <Select>
          <Control.select
            model="loan.split.fixedDuration"
            mapProps={CONTROL.MAP_PROPS}
            validators={{
              isRequired: (val) => !validator.isEmpty(val)
            }}
          >
            { times(7, index => <option value={index + 1} key={index + 1}>{index + 1 === 1 ? `${index + 1} year` : `${index + 1} years`}</option>) }
          </Control.select>
        </Select>
        <Errors
          model="loan.split.fixedDuration"
          show={ERRORS.SHOW}
          wrapper={ERRORS.WRAPPER}
          component={ERRORS.COMPONENT}
          messages={{
            isRequired: 'frequency is required',
          }}
        />
      </ControlWrapper>
  </div>
)

const AmortizationFormInterestOnly = () => (
  <div>
    <ControlWrapper
      label={{
        position: 'pre',
        text: `Interest Only Duration`
      }}
      controlStyle="stacked"
    >
      <Select>
        <Control.select
          model="loan.repayment.interestOnlyDuration"
          mapProps={CONTROL.MAP_PROPS}
          validators={{
            isRequired: (val) => !validator.isEmpty(val)
          }}
        >
          { times(5, index => <option value={index + 1} key={index + 1}>{index + 1 === 1 ? `${index + 1} year` : `${index + 1} years`}</option>) }
        </Control.select>
      </Select>
      <Errors
        model="loan.repayment.interestOnlyDuration"
        show={ERRORS.SHOW}
        wrapper={ERRORS.WRAPPER}
        component={ERRORS.COMPONENT}
        messages={{
          isRequired: 'interest only duration is required',
        }}
      />
    </ControlWrapper>
  </div>
)


const AmortizationForm = ({ splitActive, interestOnlyRepayment }) => (
  <Form
    model="loan"
    className="Form-wrapper l-container"
  >

    <div className="u-amortization-form">
      <ControlWrapper
        label={{
          position: 'pre',
          text: `Loan Amount`
        }}
        controlType="currency"
        controlStyle="stacked"
      >
        <Control.text
          model="loan.borrow.amount"
          validators={{
            isRequired: (val) => !validator.isEmpty(val),
            isNumeric: (val) => validator.isNumeric(`${accounting.unformat(val)}`),
            isBetween: (val) => accounting.unformat(val) >= 100000 && accounting.unformat(val) <= 10000000
          }}
          mapProps={CONTROL.MAP_PROPS}
          updateOn={['blur']}
        />
        <Errors
          model="loan.borrow.amount"
          show={ERRORS.SHOW}
          wrapper={ERRORS.WRAPPER}
          component={ERRORS.COMPONENT}
          messages={{
            isRequired: 'amount is required',
            isNumeric: 'amount should be numeric',
            isBetween: 'between $100,000 and $10,000,000'
          }}
        />
      </ControlWrapper>

      <ControlWrapper
        label={{
          position: 'pre',
          text: `Loan Duration`
        }}
        controlStyle="stacked"
      >
        <Select>
          <Control.select
            model="loan.mortgage.term"
            mapProps={CONTROL.MAP_PROPS}
            validators={{
              isRequired: (val) => !validator.isEmpty(val)
            }}
          >
            { times(30, index => <option value={index + 1} key={index + 1}>{index + 1 === 1 ? `${index + 1} year` : `${index + 1} years`}</option>) }
          </Control.select>
        </Select>
        <Errors
          model="loan.mortgage.term"
          show={ERRORS.SHOW}
          wrapper={ERRORS.WRAPPER}
          component={ERRORS.COMPONENT}
          messages={{
            isRequired: 'income frequency is required',
          }}
        />
      </ControlWrapper>

      <ControlWrapper
        label={{
          position: 'pre',
          text: `Payment Frequency`
        }}
        controlStyle="stacked"
      >
        <Select>
          <Control.select
            model="loan.mortgage.frequency"
            mapProps={CONTROL.MAP_PROPS}
            validators={{
              isRequired: (val) => !validator.isEmpty(val)
            }}
          >
            <option value={WEEKLY}>{S(WEEKLY).titleCase().s}</option>
            <option value={FORTNIGHTLY}>{S(FORTNIGHTLY).titleCase().s}</option>
            <option value={MONTHLY}>{S(MONTHLY).titleCase().s}</option>
          </Control.select>
        </Select>
        <Errors
          model="loan.mortgage.term"
          show={ERRORS.SHOW}
          wrapper={ERRORS.WRAPPER}
          component={ERRORS.COMPONENT}
          messages={{
            isRequired: 'frequency is required',
          }}
        />
      </ControlWrapper>
    </div>

    <div className="u-amortization-form" style={{marginBottom: '2rem'}}>
      <label><Control.checkbox model="loan.split.active" /> Fix loan</label>
    </div>

    {splitActive && <AmortizationFormFixed />}

    <div className="u-amortization-form" style={{marginBottom: '2rem'}}>
      <label><Control.checkbox model="loan.repayment.interestOnly" /> Interest Only Repayment</label>
    </div>

    {interestOnlyRepayment && <AmortizationFormInterestOnly />}
  </Form>
)

const mapStateToProps = (state) => {
  return {
    splitActive: state.loan.split.active,
    interestOnlyRepayment: state.loan.repayment.interestOnly
  }
}

export default connect(mapStateToProps)(AmortizationForm);
