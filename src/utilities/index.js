module.exports = Object.assign({},
  require('./colors'),
  require('./media-queries'),
  require('./spacing')
)
