import React from 'react';
import { PARAMETERS, STEPS } from '../constants';
import { currencyFormat } from './formatter';

const applicantTypeSummary = (type) => {
  return `
    ${type === PARAMETERS.JOINT ? `It's two of us` : `It's just me`}.
  `
}

const applicationPurposeSummary = (type, purpose) => {
  return `
    ${type === PARAMETERS.JOINT ? `We're` : `I'm`}
    ${purpose === PARAMETERS.BUYING ? `buying` : `refinancing`}.
  `
}

const propertySummary = (type, property) => {
  const possessivePronoun = `${type === PARAMETERS.JOINT ? `our` : `my` }`;
  const objectPronoun = `${type === PARAMETERS.JOINT ? `us` : `me` }`;
  const purpose = (purpose) => {
    switch(purpose) {
      case PARAMETERS.PERSONAL:
        return `${possessivePronoun} first home to live in`
      case PARAMETERS.INVESTMENT:
        return `an investment property`
      case PARAMETERS.INVESTMENT_FIRST_HOME:
        return `${possessivePronoun} first home as an investment property`
      case PARAMETERS.PERSONAL_NEW_HOME:
        return `a new place for ${objectPronoun} to live in`
      default:
        return ``
    }
  };

  const description = (description) => {
    switch(description) {
      case PARAMETERS.ESTABLISHED_HOME:
        return `an established home`
      case PARAMETERS.NEWLY_CONSTRUCTED:
        return `a newly constructed home`
      case PARAMETERS.VACANT_LAND:
        return `a vacant land to build a home`
      default:
        return ``
    }
  };

  return (
    <div>
      {`The property is worth ${currencyFormat(property.value, false)}.`}<br/>
      {`It's going to be ${purpose(property.purpose)}. `}
      {`It's ${description(property.description)} and located in ${property.state}.`}
    </div>
  )

}

const financeSummary = (type, finances) => {
  const adjective = `${type === PARAMETERS.JOINT ? `Our` : `My` }`
  return `
    ${adjective} ${finances.incomeFrequency} gross income is  ${currencyFormat(finances.income, false)} with expenses of ${currencyFormat(finances.expenses, false)} every ${finances.expensesFrequency}.
  `
}

const borrowSummary = (type, borrow) => {
  const adjective = `${type === PARAMETERS.JOINT ? `We'd` : `I'd` }`;
  return `${adjective} like to borrow ${currencyFormat(borrow.amount, false)}.`
}


const summaryGenerator = (state, step) => {
  const {
    type: {
      applicant,
      application
    },
    property,
    finances,
    borrow
  } = state.loan;

  switch(step) {
    case STEPS.APPLICANT: {
      return applicantTypeSummary(applicant)
    }
    case STEPS.PURPOSE: {
      return applicationPurposeSummary(applicant, application)
    }
    case STEPS.PROPERTY: {
      return propertySummary(applicant, property)
    }
    case STEPS.FINANCES: {
      return financeSummary(applicant, finances)
    }
    case STEPS.BORROW: {
      return borrowSummary(applicant, borrow)
    }
    default: {
      return ``
    }
  }
}

export default summaryGenerator;
