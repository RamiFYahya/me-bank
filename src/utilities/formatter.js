import accounting from 'accounting'

/**
 * Formats amount in AUD currency
 * @param {Number} amount usually in cents but sometimes whole dollars in which case, you have to pass in the 'inCents' argument as false
 * @param {Boolean} default in cents 'true'
 * @return {String} formatted amount
 */

const currencyFormat = (amount, inCents = true) => {
  const convertedAmount = inCents ? amount / 100 : amount;
  return accounting.formatMoney(convertedAmount, {
    symbol: '$',
    decimal: '.',
    thousand: ',',
    precision: '0',
    format: "%s%v"
  })
}

const formatNumber = (amount) => {
  const formattedNumber = accounting.formatNumber(amount);
  return formattedNumber === '0' ? '' : formattedNumber
}

export { currencyFormat, formatNumber }
