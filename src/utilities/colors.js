module.exports = {
  'primaryDark': '#0095cf',
  'primary': '#03a9f4',
  'primaryLight': '#81d4fa',
  'accent': '#f44336',
  'accentLight': '#ff5252',
  'secondary': '#343434',
  'canvas': '#f5f5f5',
  'text': 'rgba(0, 0, 0, 0.87)',
  'alternateText': 'rgba(255, 255, 255, 0.87)',
  'border': 'rgba(0, 0, 0, 0.27)'
};
