import Decimal from 'decimal.js'
import zipWith from 'lodash/zipWith'
import assignWith from 'lodash/assignWith'
import head from 'lodash/head'
import { getAmortizationAmount } from './amortization'
import { getPaymentFrequency, convertToCents } from '../calculators'


const REPAYMENT_OPTIONS = {
  interestOnly: false
}

const SPLIT_OPTIONS = {
  active: false
}

/**
 *
 * Internal functions
 *
 */
const customizer = (objValue, srcValue, key) => {
  return key === 'paymentNumber' ? objValue : Decimal.add(objValue, srcValue)
}

const getLoanDetails = (principal, yearlyRate, yearsDuration, frequency, repayment) => {
  const paymentFrequency = getPaymentFrequency(frequency);
  const paymentPerPeriod = getAmortizationAmount(principal, yearsDuration, yearlyRate, frequency);
  const ratePerPeriod = Decimal.div(yearlyRate, paymentFrequency).div(100);

  return {
    paymentPerPeriod,
    ratePerPeriod
  }
}

const getRemainingDurationForSplit = (split, repayment, yearsDuration) => {
  if (repayment.interestOnlyDuration > split.fixedDuration) {
    return yearsDuration - repayment.interestOnlyDuration
  }

  if (split.fixedDuration >= repayment.interestOnlyDuration) {
    return (yearsDuration - repayment.interestOnlyDuration) - (split.fixedDuration - repayment.interestOnlyDuration)
  }
}

const getAmortizationSchedule = (principal, yearsDuration, yearlyRate, frequency, split = SPLIT_OPTIONS, repayment = REPAYMENT_OPTIONS) => {
  const schedule = [];
  const paymentFrequency = getPaymentFrequency(frequency);

  const totalNumberOfPayments = yearsDuration * paymentFrequency;
  const totalNumberOfInterestOnlyPayments = repayment.interestOnly ? (repayment.interestOnlyDuration * paymentFrequency) : 0;
  const numberOfPaymentsWithFixedRate = split.active ? split.fixedDuration * getPaymentFrequency(frequency) : 0;
  const remainingDurationForSplit = getRemainingDurationForSplit(split, repayment, yearsDuration);

  let loanDetails;
  let setForfixedPeriod = false;
  let setForVariablePeriod = false;

  for (let i = 0; i < totalNumberOfPayments; i++) {
    const prevPrincipal = i === 0 ? principal : schedule[i-1].principalBalance;

    if (i < numberOfPaymentsWithFixedRate) {
      if (!setForfixedPeriod) {
        const duration = repayment.interestOnly ? yearsDuration - repayment.interestOnlyDuration : yearsDuration;
        loanDetails = getLoanDetails(convertToCents(split.fixedAmount), split.fixedRate, duration, frequency, repayment);
        setForfixedPeriod = true;
      }
    } else {
      if (!setForVariablePeriod) {
        let remainingDuration;
        if (repayment.interestOnly) {
          remainingDuration = split.active ? remainingDurationForSplit : yearsDuration - repayment.interestOnlyDuration;
        } else {
          remainingDuration = split.active ? yearsDuration - split.fixedDuration : yearsDuration;
        }
        loanDetails = getLoanDetails(prevPrincipal, yearlyRate, remainingDuration, frequency, repayment);
        setForVariablePeriod = true;
      }
    }

    const interestPayment = Decimal.mul(prevPrincipal, loanDetails.ratePerPeriod);
    const principalPayment = i < totalNumberOfInterestOnlyPayments ? Decimal(0) : Decimal.sub(loanDetails.paymentPerPeriod, interestPayment);
    const principalBalance = i < totalNumberOfInterestOnlyPayments ? prevPrincipal : Decimal.sub(prevPrincipal, principalPayment);
    const paymentPerPeriod = i < totalNumberOfInterestOnlyPayments ? interestPayment : loanDetails.paymentPerPeriod;

    schedule.push({
      paymentNumber: i+1,
      payment: paymentPerPeriod,
      principalBalance: principalBalance,
      interestPayment: interestPayment,
      principalPayment: principalPayment
    })
  }
  return schedule
}


/**
 *
 * Generates amortization table with ability to handle split / non-split loans. This is exported and should be the only one used externally
 * @principal {Number} - amount in ****#### CENTS #####*****
 * @yearsDuration {Number} - number of years
 * @yearlyRate {Number} - percentage annual interest variable rate
 * @frequency {String} - weekly, fortnightly or monthly
 * @split {Object} - Object of shape {
    active: {Boolean}
    fixedAmount: {String} - amount in whole dollars (TODO - pass in cents to make consistent with principal amount)
    fixedDuration: {String} - duration of fixed loan in years
    fixedRate: {Number} - percentage annual interest fixed rate
   }
   @repayment {Object} - Object of shape {
    interestOnly: {Boolean}
    interestOnlyDuration: {Number}
   }
 * @returns {Array} - array of schedule items. All amounts returned are {Decimal} object type
 *
 */

const generateAmortizationSchedule = (principal, yearsDuration, yearlyRate, frequency, split, repayment) => {
  let variableLoanPrincipal = principal;

  if (split.active) {
    const { fixedAmount } = split;
    const fixedAmountInCents = convertToCents(fixedAmount)
    variableLoanPrincipal = Decimal.sub(principal, fixedAmountInCents);
    var splitLoanSchedule = getAmortizationSchedule(fixedAmountInCents, yearsDuration, yearlyRate, frequency, split, repayment);
  }

  const nonSplitLoanSchedule = getAmortizationSchedule(variableLoanPrincipal, yearsDuration, yearlyRate, frequency, undefined, repayment);
  const mergedSchedule = zipWith(splitLoanSchedule, nonSplitLoanSchedule, (splitLoan, nonSplitLoan) => {
    return splitLoan ? assignWith(splitLoan, nonSplitLoan, customizer) : nonSplitLoan
  });

  return {
    payment: {
      amount: head(mergedSchedule).payment,
      frequency
    },
    schedule: mergedSchedule
  };
}



export { generateAmortizationSchedule }
