import Decimal from 'decimal.js'
import { getPaymentFrequency } from '../calculators'

/**
 *
 * To calculate payment per period, we must first calculate the Present Value of an Annuity PV Factor
 *
 * PV FACTOR FORMULA
 * PVF = 1/r – (1/r) x [1/(1+r)] ^ n
 * where:
 * r = interest rate per period
 * n = number of period covered by the loan (e.g. 30 years === 360 monthly payments)
 *
 */
const getPVFactor = (r, n) => {
  const x = Decimal.div(1, r);
  const y = Decimal.pow(Decimal.div(1, Decimal.add(1, r)), n);

  return Decimal.sub(x, Decimal.mul(x, y))
}


/**
 *
 * Calculates monthly, fortnightly or weekly amortization amount
 * @principal {Number} - amount in cents
 * @yearlyRate {Number} - percentage annual interest rate
 * @yearsDuration {Number} - number of years
 * @returns {Decimal} - amount in cents.
 *
 */
const getAmortizationAmount = (principal, period, rate, frequency) => {
  const ratePerPeriod = Decimal.div(rate, getPaymentFrequency(frequency)).div(100);
  const loanPeriod = Decimal.mul(period, getPaymentFrequency(frequency));
  const annuityFactor = getPVFactor(ratePerPeriod, loanPeriod);

  const amortizationAmount = Decimal.div(principal, annuityFactor);

  return amortizationAmount;
}

export { getAmortizationAmount }
