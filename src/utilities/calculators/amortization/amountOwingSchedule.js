import Decimal from 'decimal.js'
import { yearlyAmortizationSchedule } from './yearlyAmortizationSchedule';

/**
 *
 * Generates amortization table with ability to handle split / non-split loans. This is exported and should be the only one used externally
 * @principal {Number} - amount in ****#### CENTS #####*****
 * @yearsDuration {Number} - number of years
 * @yearlyRate {Number} - percentage annual interest variable rate
 * @frequency {String} - weekly, fortnightly or monthly
 * @split {Object} - Object of shape {
    active: {Boolean}
    fixedAmount: {String} - amount in whole dollars (TODO - pass in cents to make consistent with principal amount)
    fixedDuration: {String} - duration of fixed loan in years
    fixedRate: {Number} - percentage annual interest fixed rate
   }
   @repayment {Object} - Object of shape {
    interestOnly: {Boolean}
    interestOnlyDuration: {Number}
   }
 * @returns {Array} - array of schedule items. All amounts returned are {Decimal} object type
 *
 */

const amountOwingSchedule = (principal, term, interestRate, frequency, split, repayment) => {
  const yearlySchedule = yearlyAmortizationSchedule(principal, term, interestRate, frequency, split, repayment);
  const enhancedData = yearlySchedule.schedule.map((obj, yearlyScheduleIndex, arr) => {
    const restScheduleItems = arr.filter((scheduleItem, index) => (
      yearlyScheduleIndex < index ? scheduleItem : null
    ));
    const interestPayments = restScheduleItems.map(item => item.interestPayment);
    const totalInterestBalance = interestPayments.reduce((prev, curr) => (Decimal.add(prev, curr)), new Decimal(0));
    const amountOwing = Decimal.add(obj.principalBalance, totalInterestBalance);

    return Object.assign({}, obj, {
      amountOwing: amountOwing,
      interestOwing: totalInterestBalance
    });
  })

  return {
    payment: yearlySchedule.payment,
    schedule: enhancedData
  }
};

export { amountOwingSchedule }
