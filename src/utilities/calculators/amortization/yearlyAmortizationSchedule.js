import Decimal from 'decimal.js'
import { generateAmortizationSchedule } from './amortizationSchedule'
import { getPaymentFrequency } from '../calculators'

/**
 *
 * Generates amortization table with ability to handle split / non-split loans. This is exported and should be the only one used externally
 * @principal {Number} - amount in ****#### CENTS #####*****
 * @yearsDuration {Number} - number of years
 * @yearlyRate {Number} - percentage annual interest variable rate
 * @frequency {String} - weekly, fortnightly or monthly
 * @split {Object} - Object of shape {
    active: {Boolean}
    fixedAmount: {String} - amount in whole dollars (TODO - pass in cents to make consistent with principal amount)
    fixedDuration: {String} - duration of fixed loan in years
    fixedRate: {Number} - percentage annual interest fixed rate
   }
   @repayment {Object} - Object of shape {
    interestOnly: {Boolean}
    interestOnlyDuration: {Number}
   }
 * @returns {Array} - array of schedule items. All amounts returned are {Decimal} object type
 *
 */

const yearlyAmortizationSchedule = (principal, yearsDuration, yearlyRate, frequency, split, repayment) => {
  const amortizationSchedule = generateAmortizationSchedule(principal, yearsDuration, yearlyRate, frequency, split, repayment);
  const years = {};

  amortizationSchedule.schedule.forEach((item) => {
    const yearNumber = Math.floor((item.paymentNumber - 1) / getPaymentFrequency(frequency)) + 1;
    const year = years[yearNumber] || {};
    year.paymentNumber = yearNumber;
    year.principalPayment = Decimal.add((year.principalPayment || 0), item.principalPayment);
    year.interestPayment = Decimal.add((year.interestPayment || 0), item.interestPayment);
    year.payment = Decimal.add((year.payment || 0), item.payment);
    years[yearNumber] = year;
  })

  const schedule = [];
  for (var i = 0; i <= yearsDuration; i++) {
    let scheduleItem;
    if (i === 0) {
      scheduleItem = {
        paymentNumber: 0,
        principalPayment: 0,
        interestPayment: 0,
        payment: 0,
        principalBalance: principal
      }

    } else {
      const prevPrincipal = schedule[i-1].principalBalance;
      const principalPayment = years[i].principalPayment;
      const principalBalance = Decimal.sub(prevPrincipal, principalPayment);
      scheduleItem = Object.assign({}, years[i], {
        principalBalance: principalBalance
      })
    }

    schedule.push(scheduleItem)
  }

  return {
    payment: amortizationSchedule.payment,
    schedule
  };
}

export { yearlyAmortizationSchedule }
