import { amortization } from './amortization'
import { WEEKLY, FORTNIGHTLY, MONTHLY, YEARLY } from '../../constants/frequency'

const convertToCents = (amount) => {
  return amount * 100
}

const convertToMonthly = (frequency, amount) => {
  switch (frequency) {
    case WEEKLY:
      return parseFloat(convertToCents(amount)) * 52 / 12
    case FORTNIGHTLY:
      return parseFloat(convertToCents(amount)) * 26 / 12
    case MONTHLY:
      return parseFloat(convertToCents(amount))
    case YEARLY:
      return parseFloat(convertToCents(amount)) / 12
    default:
      return convertToCents(amount)
  }
}

const calculateBorrowAmount = (preloanData) => {
  const {
    loan: {
      amount
    },
    finances: {
      deposit: {
        amount: deposit
      }
    }
  } = preloanData;

  return convertToCents(amount - deposit); // convert to cents
}


/**
 *
 * Mock stamp duty calculator until API finalised
 *
 */

const calculateStampDuty = (preloanData) => {
  const amount = calculateBorrowAmount(preloanData);

  return amount * 0.055;
}


/**
 *
 * Mock LMI calculator until API finalised
 *
 */

const calculateLMI = (preloanData) => {
  const amount = calculateBorrowAmount(preloanData);

  return amount * 0.015;
}

const calculateMonthlyRepayment = (state) => {
  const {
    loan,
    mortgage: {
      term,
      interestRate
    }
  } = state;

  const amountToBorrow = loan.borrow.amount;
  return amortization(amountToBorrow, interestRate, term);
}

const getPaymentFrequency = (frequency) => {
  switch (frequency) {
    case WEEKLY:
      return 52;
    case FORTNIGHTLY:
      return 26;
    case MONTHLY:
      return 12;
    default:
      return 12;
  }
}

export { convertToMonthly, calculateBorrowAmount, calculateMonthlyRepayment, getPaymentFrequency, convertToCents, calculateStampDuty, calculateLMI }
