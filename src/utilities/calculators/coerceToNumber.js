import mapValues from 'lodash/mapValues';

const decimalToNumber = (item) => {
  return mapValues(item, (value, key) => typeof value === 'object' ? value.round().toNumber() : value)
}

/**
 *
 * Rounds and Coerces repayment schedule to primitive number instead of Decimal.js type
 * Used to overcome tooltips issue - feels a bit dirty - need to rethink and refactor accordingly
 *
 */

const coerceToNumber = (schedule) => schedule.map(item => decimalToNumber(item))

export default coerceToNumber
