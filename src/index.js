import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import './utilities/polyfills'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import muiTheme from './styles'
import App from './App'
import store from './store'

// Styles
import './styles/index.css'
import './styles/app.css'
import './styles/form.css'
import './styles/layout.css'

render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={muiTheme}>
      <App />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
)
