import * as types from '../constants/actionTypes'
import fetch from 'isomorphic-fetch'

export const activateStep = (step) => {
  return {
    type: types.ACTIVATE_STEP,
    id: step
  }
}

export const completeStep = (step) => {
  return {
    type: types.COMPLETE_STEP,
    id: step
  }
}

export const editStep = (step) => {
  return {
    type: types.EDIT_STEP,
    id: step
  }
}

export const changeProduct = (product) => {
  return {
    type: types.CHANGE_PRODUCT,
    name: product
  }
}

export const changeTerm = (term) => {
  return {
    type: types.CHANGE_TERM,
    term: term
  }
}

export const requestInterest = () => {
  return {
    type: types.REQUEST_INTEREST
  }
}

export const receiveInterest = (json) => {
  return {
    type: types.RECEIVE_INTEREST,
    interestRate: parseFloat(json.data.interestRate),
    receivedAt: Date.now()
  }
}

export const changeOption = (model, value) => {
  return {
    type: types.CHANGE_OPTION,
    model,
    value
  }
}

/**
  TODO:
  - Need to check whether there's a request in flight -
  - if yes don't dispatch another fetch
 */
export const fetchInterest = () => {
  return (dispatch, getState) => {
    const state = getState();

    const {
      mortgage: {
        term,
        activeProduct
      }
    } = state;
    dispatch(requestInterest(term))

    return fetch('http://localhost:3001/interestrate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        product: activeProduct
      })
    })
      .then(response => {
        return response.json()
      })
      .then(json => dispatch(receiveInterest(json)))
  }
}
