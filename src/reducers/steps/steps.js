import { EDIT_STEP, COMPLETE_STEP, ACTIVATE_STEP } from '../../constants/actionTypes'

const initialState = {};

const steps = (state = initialState, action) => {
  switch (action.type) {
    case EDIT_STEP:
      return {
        ...state,
        [action.id]: {
          ...state[action.id],
          completed: false
        }
      }
    case COMPLETE_STEP:
      return {
        ...state,
        [action.id]: {
          ...state[action.id],
          completed: true
        }
      }
    case ACTIVATE_STEP:
      return {
        ...state,
        [action.id]: {
          ...state[action.id],
          active: true
        }
      }
    default:
      return state
  }
}

export default steps
