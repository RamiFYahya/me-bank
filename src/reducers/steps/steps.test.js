import deepFreeze from 'deep-freeze';
import reducer from './steps';
import { EDIT_STEP, COMPLETE_STEP, ACTIVATE_STEP } from '../../constants/actionTypes';

describe('steps reducer', () => {
  it('should return the initial state', () => {
    const action = {};
    const stateBefore = undefined;
    const stateAfter = {};
    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  })

  it('should activate step for editing', () => {
    const action = {
      type: EDIT_STEP,
      id: 'applicant'
    };
    const stateBefore = {
      applicant: {
        active: false,
        completed: true
      },
      purpose: {
        active: false,
        completed: false
      }
    };
    const stateAfter = {
      applicant: {
        active: false,
        completed: false
      },
      purpose: {
        active: false,
        completed: false
      }
    };

    deepFreeze(stateBefore);

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  })

  it('should mark step completed', () => {
    const action = {
      type: COMPLETE_STEP,
      id: 'applicant'
    };
    const stateBefore = {
      applicant: {
        active: false,
        completed: false
      },
      purpose: {
        active: false,
        completed: false
      }
    };
    const stateAfter = {
      applicant: {
        active: false,
        completed: true
      },
      purpose: {
        active: false,
        completed: false
      }
    };

    deepFreeze(stateBefore);

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  })

  it('should activate step', () => {
    const action = {
      type: ACTIVATE_STEP,
      id: 'purpose'
    };
    const stateBefore = {
      applicant: {
        active: true,
        completed: false
      },
      purpose: {
        active: false,
        completed: false
      }
    };
    const stateAfter = {
      applicant: {
        active: true,
        completed: false
      },
      purpose: {
        active: true,
        completed: false
      }
    };

    deepFreeze(stateBefore);

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  })
})
