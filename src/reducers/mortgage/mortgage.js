import { FLEXIBLE, FLEXIBLEMP } from '../../constants/productCodes'
import { CHANGE_PRODUCT, CHANGE_TERM, REQUEST_INTEREST, RECEIVE_INTEREST } from '../../constants/actionTypes'

const initialMortgage = {
  isFetching: false
};

export default function mortgage(state = initialMortgage, action) {
  switch (action.type) {
    case CHANGE_TERM:
      return {
        ...state,
        term: action.term
      }
    case CHANGE_PRODUCT:
      return {
        ...state,
        split: {
          ...state.split,
          available: action.name === FLEXIBLE || action.name === FLEXIBLEMP
        },
        activeProduct: action.name
      }
    case REQUEST_INTEREST:
      return {
        ...state,
        isFetching: true
      }
    case RECEIVE_INTEREST:
      return {
        ...state,
        isFetching: false,
        interestRate: action.interestRate
      }
    default:
      return state
  }
}
