import deepFreeze from 'deep-freeze';
import reducer from './mortgage'
import { BASIC, FLEXIBLE, FLEXIBLEMP } from '../../constants/productCodes'
import * as types from '../../constants/actionTypes'

describe('mortgage reducer', () => {
  it('should return the initial state', () => {
    const action = {};
    const stateBefore = undefined;
    const stateAfter = {
      isFetching: false
    };

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  });

  it('should change the loan term', () => {
    const action = {
      type: types.CHANGE_TERM,
      term: 20
    };
    const stateBefore = {
      term: 27
    };
    const stateAfter = {
      term: 20
    };

    deepFreeze(stateBefore);

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  });

  it('should handle changing available product from Basic to Flexible', () => {
    const action = {
      type: types.CHANGE_PRODUCT,
      name: FLEXIBLE
    };
    const stateBefore = {
      activeProduct: BASIC,
      split: {
        available: false,
        fixedAmount: '',
        fixedDuration: 1
      },
    };
    const stateAfter = {
      activeProduct: FLEXIBLE,
      split: {
        available: true,
        fixedAmount: '',
        fixedDuration: 1
      },
    };

    deepFreeze(stateBefore);

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  });

  it('should handle changing available product from Flexible to Basic', () => {
    const action = {
      type: types.CHANGE_PRODUCT,
      name: BASIC
    };
    const stateBefore = {
      activeProduct: FLEXIBLE,
      split: {
        available: true,
        fixedAmount: '',
        fixedDuration: 1
      },
    };
    const stateAfter = {
      activeProduct: BASIC,
      split: {
        available: false,
        fixedAmount: '',
        fixedDuration: 1
      },
    };

    deepFreeze(stateBefore);

    expect(
      reducer(stateBefore, action)
    ).toEqual(stateAfter)
  });
})
