import { combineReducers } from 'redux';
import { createForms, modelReducer } from 'react-redux-form';
import steps from './steps';
import mortgage from './mortgage';

const onlineHomeLoans = combineReducers({
  steps,
  loan: combineReducers({
    type: modelReducer('loan.type', {}),
    ...createForms({
      property: {},
      finances: {},
      borrow: {},
      split: {},
      mortgage: {},
      repayment: {}
    }, 'loan')
  }),
  mortgage
})

export default onlineHomeLoans
