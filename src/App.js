import React from 'react';
import ProgressBar from './components/ProgressBar';
import Header from './components/Header';
import MultiStepForm from './containers/MultiStepForm';
import Overview from './containers/Overview';
import header from './data/header';

const App = () => {
  return (
    <div className="App">
      <ProgressBar percentageCompleted={2} />
      <Header title={header.title} subtitle={header.subtitle} />
      <MultiStepForm />
      <Overview />
    </div>
  )
};

export default App;
