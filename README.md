# Online Home Loans - Presentation Layer
The code contained within this repository is for the presentation layer of MeBank's Online Home Loans project. Currently, we're only doing custom front-end development only for the pre-application screens. The pre-application is where a potential customer enters minimal detail to work out an approximation of their home loan application. It gives an indication of how much the monthly payments will be, the interest rate per product, ability to simulate a split loan.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). You will find some information on how to perform common tasks [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
___
## Table of contents
- Tech Stack
 - [JS](#js)
 - [CSS](#css)
 - [Build Tools](#build-tools)
 - [Eco System](#eco-system)
- [Version Control](#version-control)
 - [Git Workflow](#git-workflow)
 - [Deployment](#deployment)
- [Coding Guidelines](#coding-guidelines)
 - [Folder Structure](#folder-structure)
 - [JS](#js-guidelines)
 - [CSS](#css-guidelines)
- [Future Plans](future-plans)
 - [To-Dos](#todos)

---
##[Front-end Tech Stack](#tech-stack)
The pre application is built on top of the latest technologies and best practices in the front-end realm.
#### JS Frameworks
The JS framework of choice for this application is [ReactJS](https://facebook.github.io/react/) for building declarative UI and [Redux](http://redux.js.org/) for application state management.
#### ES6
Javascript in this application is written using [ES6](http://es6-features.org/) - the latest stable specification of [ECMAScript](http://www.ecma-international.org/ecma-262/6.0/).
#### CSS Processor
We're using [PostCSS](http://postcss.org/) for writing future-proof CSS.
#### Build Tool
[Webpack](https://webpack.github.io/) is the module bundler of choice for this application. Webpack takes modules with dependencies and generates static assets representing those modules.
#### Eco System
Webpack and the build tools integrated into this application depends on [NodeJS](https://nodejs.org/en/) being available on the developer's machine. [NPM](https://www.npmjs.com/) is the package manager of choice.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

---
## Version Control
By default, when you run `npm test`, Jest will only run the tests related to files changed since the last commit. This is an optimization designed to make your tests runs fast regardless of how many tests you have. However it assumes that you don’t often commit the code that doesn’t pass the tests.

Jest will always explicitly mention that it only ran tests related to the files changed since the last commit. You can also press `a` in the watch mode to force Jest to run all tests.

#### Git Workflow
In order to maximise the efficiency of collaboration amongst front-end developers on this project, we've put in place a [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) Workflow.
#### Deployment
The output from this code is being hosted inside PEGA. Currently, it's a manual process of running `npm run build` and passing the build files to PEGA developers via an attachment in JIRA. Ideally, this process should be automated to push the build code into PEGA environment programatically.

---
## Coding Guidelines
There are certain coding standards we need to adhere to. This coding standards are not set in stone and can evolve overtime according to best practices, peer reviews and discussions. Any agreed changes to the coding standards should be updated here.
#### Folder Structure
```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    actions/
    assets/
    components/
      ComponentName/
        ComponentName.js
        ComponentName.test.js
        ComponentName.css
        index.js
    constants/
    containers/
      ContainerName.js
    data/
    reducers/
      reducerName/
        reducerName.js
        reducerName.test.js
        index.js
    styles/
    utilities/
```
#### JS
ESLint has been configured to display warnings when code doesn't adhere to the rules in place. The ESLint configuration extends [eslint-config-react-app](https://www.npmjs.com/package/eslint-config-react-app). You can also display lint output in your editor of choice. See [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#displaying-lint-output-in-the-editor) for setup instructions.
#### CSS
Styling for components are co-located in a component's folder. The only exception to this is if you need to create global styles, resets, layout etc. In this case you can add these to the `styles/` folder.

---
## Future Plans
There's a few things missing to make the process of going from `development -> test -> production` frictionless and efficient. Right now we don't have any automated processes to manage this. Our hosting environment is **PEGA**. Code is deployed into PEGA manually. Front-end developer runs `npm run build` then hands over the JS and CSS files to a PEGA developer with access to upload custom code into PEGA environment.

#### To Do
 - Investigate whether we have infrastructure in place to facilitate Continuous Integration. If not to production, then at least to a `test` environment.
 - Setup BitBucket to trigger a `pull request` when merging a feature back into `develop` branch. Pull requests should ideally be peer reviewed.
 - Current unit tests cover only `reducers`. Components should also be unit tested.